misc-gitlab-scripts
===================

This repo contains various scripts I use for dealing with gitlab.
Before beginning, you'll need to install & configure ``python-gitlab``
as described at:
https://python-gitlab.readthedocs.io/en/stable/cli.html#configuration

Each utility has some (terse) help.
