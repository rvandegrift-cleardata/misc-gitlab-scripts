# Copyright 2020 ClearDATA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0

import argparse

import gitlab

parser = argparse.ArgumentParser(description='Get/Set project tags (aka topics) by group and project name.')
parser.add_argument('--gitlab-cfg', '-g', type=str,
                    help='Name of python-gitlab config')
parser.add_argument('project_id', type=str,
                    help='ID of project')
parser.add_argument('tags', type=str, nargs='?', default='',
                    help='Comma-delimited list of tags to manipulate.')

op = parser.add_mutually_exclusive_group()
op.add_argument('--get', action='store_false',
                help='Get the current tags.  This is the default.')
op.add_argument('--set', action='store_true',
                help='Set the tags to the given list.')
op.add_argument('--add', action='store_true',
                help='Add all of the given tags to the list.')
op.add_argument('--remove', action='store_true',
                help='Remove all of the given tags from the list.')
op.add_argument('--delete', action='store_true',
                help='Delete all project tags')

args = parser.parse_args()

g = gitlab.Gitlab.from_config(args.gitlab_cfg)


def main():
    project = g.projects.get(args.project_id)

    tags = args.tags.split(',')

    if args.add:
        project.tag_list.extend(tags)

    elif args.set:
        project.tag_list = tags

    elif args.remove:
        for t in tags:
            try:
                project.tag_list.remove(t)
            except ValueError:
                pass            # tag is already missing

    elif args.delete:
        project.tag_list = []

    project.save()

    print(','.join(project.tag_list))


if __name__ == '__main__':
    main()
