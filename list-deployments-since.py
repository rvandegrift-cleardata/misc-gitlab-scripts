# Copyright 2019 ClearDATA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0

import argparse
import datetime
import pprint

import gitlab

parser = argparse.ArgumentParser(description='List deployments since a date for all projects in the given group.')
parser.add_argument('group', type=str, help='Path to desired group')
parser.add_argument('since', type=datetime.datetime.fromisoformat,
                    help='Date to start, in iso format')
parser.add_argument('until', type=datetime.datetime.fromisoformat, nargs='?',
                    default=datetime.datetime.utcnow().isoformat())
parser.add_argument('--env', type=str, help='Limit to a target environment')
parser.add_argument('--gitlab-cfg', '-g', type=str,
                    help='Name of python-gitlab config')
args = parser.parse_args()

g = gitlab.Gitlab.from_config(args.gitlab_cfg)


def main():
    group = g.groups.get(args.group)

    result = {}

    for project_id in group.projects.list(as_list=False):
        project = g.projects.get(project_id.id)

        for d in project.deployments.list(as_list=False):
            if args.env and d.environment['name'] != args.env:
                continue

            created_at = datetime.datetime.fromisoformat(
                d.created_at.split('T')[0]
            )
            if created_at >= args.since and created_at <= args.until:
                result[d.created_at] = project.name

    print('Found %d releases:' % len(result))
    pprint.pprint(result)


if __name__ == '__main__':
    main()
