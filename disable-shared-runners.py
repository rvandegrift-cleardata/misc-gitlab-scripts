# Copyright 2019 ClearDATA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0

import argparse

import gitlab

parser = argparse.ArgumentParser(description='Disable shared runners')
parser.add_argument('--gitlab-cfg', '-g', type=str,
                    help='Name of python-gitlab config')
args = parser.parse_args()

g = gitlab.Gitlab.from_config(args.gitlab_cfg)


def main():
    for group in g.groups.list(as_list=False):
        group = g.groups.get(group.id)

        for project_id in group.projects.list(as_list=False):
            project = g.projects.get(project_id.id)
            if project.shared_runners_enabled:
                print('%s/%s' % (
                    project.attributes['namespace']['full_path'],
                    project.name,
                ))
                project.shared_runners_enabled = False
                project.save()


if __name__ == '__main__':
    main()
