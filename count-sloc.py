# Copyright 2021 ClearDATA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0

import argparse
from collections import defaultdict
import json
import os
import pprint
import tempfile
import time

import git
import gitlab
import pygount
import urllib.parse

parser = argparse.ArgumentParser(description='Analyze lines of code in every repo')
parser.add_argument('--gitlab-cfg', '-g', type=str,
                    help='Name of python-gitlab config')
parser.add_argument('data_file', type=str,
                    help='Filename to use for storing incremental data')
args = parser.parse_args()

g = gitlab.Gitlab.from_config(args.gitlab_cfg)

data = None


def json_pygount_dumper(o):
    if isinstance(o, pygount.LanguageSummary):
        return {
            'language': o.language,
            'file_count': o.file_count,
            'code_count': o.code_count,
            'documentation_count': o.documentation_count,
            'empty_count': o.empty_count,
            'string_count': o.string_count,
        }

    raise TypeError('unsupported type')


def load_data():
    global data
    try:
        with open(args.data_file, 'r') as f:
            data = json.load(f)
    except Exception:
        data = {}

    return data


def write_data():
    global data
    with open(args.data_file, 'w') as f:
        json.dump(data, f, default=json_pygount_dumper)


def find_repos(data):
    for project in g.projects.list(as_list=False, per_page=100):
        proj_path = project.attributes['path_with_namespace']
        if proj_path not in data:
            data[proj_path] = {
                'url': project.attributes['http_url_to_repo'],
                'code_summary': {},
            }
            print(f'\td+ {proj_path}')
        else:
            print(f'\td. {proj_path}')

    return data


def clone_repo_via_http(project_url, target):
    # munge the url's netloc to insert the auth token
    u = list(urllib.parse.urlparse(project_url))
    u[1] = f'count-sloc:{g.private_token}@{u[1]}'
    url = urllib.parse.urlunparse(u)

    n = 0
    m = 3
    while n < m:
        try:
            git.Repo.clone_from(url, target, multi_options=['--depth=1'])
        except git.exc.GitCommandError:
            time.sleep(0.1)
            n += 1
        else:
            return

    if n == 3:
        print(f'ERROR: failed to clone {project_url}')


def count_lines(project_url):
    with tempfile.TemporaryDirectory() as tmp:
        clone_repo_via_http(project_url, tmp)

        proj_summary = pygount.ProjectSummary()
        for root, dirs, files in os.walk(tmp):
            if '.git' in dirs:
                dirs.remove('.git')
            for f in files:
                src_f = os.path.join(root, f)
                try:
                    a = pygount.SourceAnalysis.from_file(src_f, "")
                    proj_summary.add(a)
                except FileNotFoundError:
                    # committed symlinks break this
                    print(f'ERROR: failed to analyze {src_f}')

        return proj_summary.language_to_language_summary_map


def summary_add_lang_data(summary, details):
    lang = details['language']
    if lang not in summary:
        summary[lang] = defaultdict(int)
        summary[lang]['language'] = lang

    for k, v in details.items():
        if isinstance(v, int):
            summary[lang][k] += details[k]


def main():
    global data

    load_data()
    if not data:
        print('discovering repos...')
        data = find_repos(data)
        write_data()

    print('analyzing repo contents...')
    i = 0
    for proj_path, proj_data in data.items():
        i += 1

        if proj_data['code_summary']:
            print(f'\ta. {proj_path}\t\t[{i}/{len(data)}]')
            continue

        print(f'\ta+ {proj_path}\t\t[{i}/{len(data)}]')
        proj_data['code_summary'] = count_lines(proj_data['url'])
        write_data()

    print('generating summary data...')
    summary = {}
    for proj_path, proj_data in data.items():
        for lang, details in proj_data['code_summary'].items():
            summary_add_lang_data(summary, details)

    pprint.pprint({k: dict(v) for k, v in summary.items()})

    print('generating totals....')
    totals = defaultdict(int)
    for lang, details in summary.items():
        for k, v in details.items():
            if not isinstance(v, int):
                continue

            totals[k] += v

    pprint.pprint(dict(totals))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        write_data()
